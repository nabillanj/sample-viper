//
//  MovieRouter.swift
//  ExampleVIPER
//
//  Created by nabilla nurjannah on 04/02/19.
//  Copyright © 2019 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class MovieRouter: MovieRouterProtocol {
    static func createModule() -> MovieViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "MovieViewController") as! MovieViewController
        
        let presenter: MovieProtocol & MoviePresenterProtocol = MoviePresenter()
        var interactor: MovieInteractorProtocol = MovieInteractor()
        let router: MovieRouterProtocol = MovieRouter()
        
        view.interactor = interactor
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}
