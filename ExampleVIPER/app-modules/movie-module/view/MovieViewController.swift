//
//  MovieViewController.swift
//  ExampleVIPER
//
//  Created by nabilla nurjannah on 04/02/19.
//  Copyright © 2019 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class MovieViewController: BaseViewController {

    //MARK: Outlet
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Variable
    var movieArray: Array<MovieModel> = Array()
    var interactor: MovieInteractorProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showLoadingBar(view: self.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Movie Module"
        interactor?.fetchMovie()
    }
}

extension MovieViewController: MovieViewProtocol {
    func showMovie(arrayOfMovie: Array<MovieModel>) {
        movieArray = arrayOfMovie
        tableView.reloadData()
        hideLoadingBar(view: self.view)
    }
    
    func showError() {
        hideLoadingBar(view: self.view)
    }
}

extension MovieViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieCell
        cell.bind(movie: movieArray[indexPath.row])
        
        return cell
    }
}

class MovieCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var movieImageView: UIImageView!
    
    func bind(movie: MovieModel) {
        titleLabel.text = movie.title
        descriptionLabel.text = movie.brief
//        imageView?.af_setImage(withURL: URL(string: movie.image!)!)
        Alamofire.request(URL(string: movie.image!)!).responseData { (response) in
            if response.error == nil {
                print(response.result)
                if let data = response.data {
                    self.movieImageView.image = UIImage(data: data)
                }
            }
        }
    }
}
