//
//  MoviePresenter.swift
//  ExampleVIPER
//
//  Created by nabilla nurjannah on 04/02/19.
//  Copyright © 2019 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class MoviePresenter: MoviePresenterProtocol {
    var view: MovieViewProtocol?
    var interactor: MovieInteractorProtocol?
    var router: MovieRouterProtocol?
    
    func movieFetchedSuccess(modelArray: Array<MovieModel>) {
        view?.showMovie(arrayOfMovie: modelArray)
    }
    
    func moviewFetchedFailed() {
        view?.showError()
    }
}

extension MoviePresenter: MovieProtocol {
    func loadMovieData() {
        interactor?.fetchMovie()
    }
}
