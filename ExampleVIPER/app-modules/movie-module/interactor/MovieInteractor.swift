//
//  MovieInteractor.swift
//  ExampleVIPER
//
//  Created by nabilla nurjannah on 04/02/19.
//  Copyright © 2019 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class MovieInteractor: MovieInteractorProtocol {
    var presenter: MoviePresenterProtocol?
    
    func fetchMovie() {
        Alamofire.request(API_MOVIE_LIST).responseJSON { response in
            if (response.response?.statusCode == 200) {
                if let json = response.result.value as AnyObject? {
                    let arrayResponse = json["movie_list"] as! NSArray
                    let arrayOfObject = Mapper<MovieModel>().mapArray(JSONArray: arrayResponse as! [[String: Any]])
                    self.presenter?.movieFetchedSuccess(modelArray: arrayOfObject)
                }
            }else {
                self.presenter?.moviewFetchedFailed()
            }
        }
    }
    
    
}
