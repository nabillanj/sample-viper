//
//  MovieModel.swift
//  ExampleVIPER
//
//  Created by nabilla nurjannah on 04/02/19.
//  Copyright © 2019 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import ObjectMapper

private let ID = "id"
private let TITLE = "title"
private let BRIEF = "brief"
private let IMAGE = "image_url"

class MovieModel: Mappable {
    
    internal var id: String?
    internal var title: String?
    internal var brief: String?
    internal var image: String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        id <- map[ID]
        title <- map[TITLE]
        brief <- map[BRIEF]
        image <- map[IMAGE]
    }
}
