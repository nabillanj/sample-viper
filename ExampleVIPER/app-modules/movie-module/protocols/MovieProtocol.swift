//
//  MovieProtocol.swift
//  ExampleVIPER
//
//  Created by nabilla nurjannah on 04/02/19.
//  Copyright © 2019 Nabilla Nurjannah. All rights reserved.
//

import UIKit

protocol MovieProtocol: class {
    var view: MovieViewProtocol? {get set}
    var interactor: MovieInteractorProtocol?{get set}
    var router: MovieRouterProtocol?{get set}
    func loadMovieData()
}

protocol MovieViewProtocol {
    func showMovie(arrayOfMovie: Array<MovieModel>)
    func showError()
}
protocol MovieRouterProtocol{
    static func createModule() -> MovieViewController
}

protocol MovieInteractorProtocol {
    var presenter: MoviePresenterProtocol? {get set}
    func fetchMovie()
}
protocol MoviePresenterProtocol {
    func movieFetchedSuccess(modelArray: Array<MovieModel>)
    func moviewFetchedFailed()
}


