//
//  NoticeViewController.swift
//  ExampleVIPER
//
//  Created by nabilla nurjannah on 04/02/19.
//  Copyright © 2019 Nabilla Nurjannah. All rights reserved.
//
  
import UIKit

//Todo: Describe code
class NoticeViewController: BaseViewController {
    //MARK: Outlet
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Variable
    var presenter: ViewToPresenterProtocol?
    var noticeArray: Array<NoticeModel> = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Notice Module"
        tableView.tableFooterView = UIView()
        showLoadingBar(view: self.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.startFetchingNotice()
    }
}

extension NoticeViewController: PresenterToViewProtocol {
    func showNotice(noticeArray: Array<NoticeModel>) {
        hideLoadingBar(view: self.view)
        self.noticeArray = noticeArray
        self.tableView.reloadData()
    }
    
    func showError() {
        hideLoadingBar(view: self.view)
        let alert = UIAlertController(title: "Alert", message: "Proble occured", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension NoticeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return noticeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "noticeCell", for: indexPath) as! NoticeCell
        cell.bind(notice: noticeArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showMovieController(navigationController: navigationController!)
    }
}

class NoticeCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func bind(notice: NoticeModel){
        titleLabel.text = notice.title
        descriptionLabel.text = notice.brief
    }
}
