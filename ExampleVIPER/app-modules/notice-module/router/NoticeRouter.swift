//
//  NoticeRouter.swift
//  ExampleVIPER
//
//  Created by nabilla nurjannah on 04/02/19.
//  Copyright © 2019 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class NoticeRouter: PresenterToRouterProtocol {
    static func createModule() -> NoticeViewController {
        let view = mainstoryboard.instantiateViewController(withIdentifier: "NoticeViewController") as! NoticeViewController
        
        //Todo: Learn thissss
        let presenter: ViewToPresenterProtocol & InteractorToPresenterProtocol = NoticePresenter()
        let interactor: PresenterToInteractorProtocol = NoticeInteractor()
        let router: PresenterToRouterProtocol = NoticeRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
    }
    
    static var mainstoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    func pushToMovieScreen(navigationController: UINavigationController) {
        let movieModule = MovieRouter.createModule()
        navigationController.pushViewController(movieModule, animated: true)
    }
    

}
