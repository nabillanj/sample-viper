//
//  NoticeProtocol.swift
//  ExampleVIPER
//
//  Created by nabilla nurjannah on 04/02/19.
//  Copyright © 2019 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import Foundation

//Use in Notice Presenter
protocol ViewToPresenterProtocol: class {
    //Todo: Describe code
    
    //Search: search {get set}
    var view: PresenterToViewProtocol? {get set}
    var interactor: PresenterToInteractorProtocol? {get set}
    var router: PresenterToRouterProtocol? {get set}
    func startFetchingNotice()
    func showMovieController(navigationController: UINavigationController)
}

//Use in View Controller
protocol PresenterToViewProtocol: class {
    func showNotice(noticeArray: Array<NoticeModel>)
    func showError()
}

//Use in Router
protocol PresenterToRouterProtocol : class {
    static func createModule() -> NoticeViewController
    func pushToMovieScreen(navigationController: UINavigationController)
}

//Use in Notice Interactor
protocol PresenterToInteractorProtocol: class {
    var presenter: InteractorToPresenterProtocol? {get set}
    func fetchNotice()
}

//Use in Presenter
protocol InteractorToPresenterProtocol: class {
    func noticeFetchedSuccess(noticeModelArray: Array<NoticeModel>)
    func noticeFetchFailed()
}
