//
//  NoticeInteractor.swift
//  ExampleVIPER
//
//  Created by nabilla nurjannah on 04/02/19.
//  Copyright © 2019 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class NoticeInteractor: PresenterToInteractorProtocol {
    //Todo: Describe code
    var presenter: InteractorToPresenterProtocol?
    
    func fetchNotice() {
        Alamofire.request(API_NOTICE_LIST).responseJSON { response in
            if (response.response?.statusCode == 200) {
                if let json = response.result.value as AnyObject? {
                    let arrayResponse = json["notice_list"] as! NSArray
                    let arrayObject = Mapper<NoticeModel>().mapArray(JSONArray: arrayResponse as! [[String : Any]]);
                    self.presenter?.noticeFetchedSuccess(noticeModelArray: arrayObject)
                }
            }else {
                self.presenter?.noticeFetchFailed()
            }
        }
    }
    
}
