//
//  AppUtils.swift
//  ExampleVIPER
//
//  Created by nabilla nurjannah on 04/02/19.
//  Copyright © 2019 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import SVProgressHUD

func showLoadingBar(view: UIView) {
    view.isUserInteractionEnabled = false
    SVProgressHUD.show()
}

func hideLoadingBar(view: UIView) {
    view.isUserInteractionEnabled = true
    SVProgressHUD.dismiss()
}
